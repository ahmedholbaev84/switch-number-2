import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Доступные операции:");
        System.out.println("1. Сложение (+)");
        System.out.println("2. Вычитание (-)");
        System.out.println("3. Умножение (*)");
        System.out.println("4. Деление (/)");

        System.out.print("Выберите номер операции (1-4): ");
        int operation = scanner.nextInt();

        System.out.print("Введите первое число: ");
        int numOne = scanner.nextInt();

        System.out.print("Введите второе число: ");
        int numTwo = scanner.nextInt();

        int result = 0;

        switch (operation) {
            case 1:
                result = numOne + numTwo;
                System.out.println("Результат сложения: " + result);
                break;
            case 2:
                result = numOne - numTwo;
                System.out.println("Результат вычитания: " + result);
                break;
            case 3:
                result = numOne * numTwo;
                System.out.println("Результат умножения: " + result);
                break;
            case 4:
                if (numTwo != 0) {
                    result = numOne / numTwo;
                    System.out.println("Результат деления: " + result);
                } else {
                    System.out.println("Ошибка: деление на ноль невозможно!");
                }
                break;
            default:
                System.out.println("Ошибка: неверный выбор операции! Пожалуйста, выберите от 1 до 4.");
        }

        scanner.close();
    }
}
